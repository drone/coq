from functools import wraps
from urllib.parse import quote_plus

from flask import redirect, session, request, abort, g

from . import FranceConnect


def auth_required(fc):
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            if fc.get_user_id():
                return func(*args, **kwargs)
            abort(403)
        return wrapper
    return decorator


class FranceConnectFlask(FranceConnect):

    def get_connected_url(self):
        return '{}?next={}'.format(super().get_connected_url(),
                                   quote_plus(request.args.get('next', '')))

    def get_disconnected_url(self):
        return '{}?next={}'.format(super().get_disconnected_url(),
                                   quote_plus(request.args.get('next', '')))

    def login_view(self):
        return redirect(self.get_login_url())

    def logout_view(self):
        if 'fc_tokens' in session:
            id_token = session['fc_tokens'].pop('id_token')
            del session['fc_tokens']
            g.user_details = None
            if id_token:
                return redirect(self.get_logout_url(id_token))
        return redirect(self.get_disconnected_url())

    def authorize_view(self):
        if request.args.get('state') != self.state:
            abort(401, '"state" param does not match your settings.')
        code = request.args.get('code')
        tokens = self.get_tokens(code)
        if tokens:
            session['fc_tokens'] = tokens
            return redirect(request.args.get('next'))
        abort(401, 'No token was authorized.')

    def get_user_details(self):
        if 'user_details' not in g:
            g.user_details = super().get_user_details(
                session.get('fc_tokens', {}).get('access_token'))
        return g.user_details

    def get_user_id(self):
        return self.get_user_details().get('sub')
