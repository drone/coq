from uuid import uuid4
from urllib.parse import urlencode

import requests


class FranceConnect:
    ROOT_URL = 'https://fcp.integ01.dev-franceconnect.fr/api/v1/'
    AUTHORIZE_URL = '{}authorize'.format(ROOT_URL)
    TOKEN_URL = '{}token'.format(ROOT_URL)
    USERINFO_URL = '{}userinfo'.format(ROOT_URL)
    LOGOUT_URL = '{}logout'.format(ROOT_URL)

    def __init__(self, client_id, client_secret, connected_url,
                 disconnected_url, **kwargs):
        self.client_id = client_id
        self.client_secret = client_secret
        self.connected_url = connected_url
        self.disconnected_url = disconnected_url
        self.state = kwargs.get('state', uuid4().hex)
        self.nonce = kwargs.get('nonce', uuid4().hex)
        self.scope = kwargs.get('scope', 'openid')

    def get_connected_url(self):
        return (self.connected_url()
                if callable(self.connected_url) else self.connected_url)

    def get_disconnected_url(self):
        return (self.disconnected_url()
                if callable(self.disconnected_url) else self.disconnected_url)

    def get_login_url(self):
        data = {
            'response_type': 'code',
            'client_id': self.client_id,
            'state': self.state,
            'nonce': self.nonce,
            'redirect_uri': self.get_connected_url(),
            'scope': self.scope,
        }
        return '{}?{}'.format(self.AUTHORIZE_URL, urlencode(data))

    def get_logout_url(self, id_token):
        data = {
            'id_token_hint': id_token,
            'state': self.state,
            'post_logout_redirect_uri': self.get_disconnected_url(),
        }
        return '{}?{}'.format(self.LOGOUT_URL, urlencode(data))

    def get_tokens(self, code):
        data = {
            'grant_type': 'authorization_code',
            'redirect_uri': self.get_connected_url(),
            'client_id': self.client_id,
            'client_secret': self.client_secret,
            'code': code,
        }
        response = requests.post(self.TOKEN_URL, data=data)
        if response.status_code != 200:
            return {}
        return response.json()

    def get_auth_headers(self, access_token):
        return {
            'Authorization': 'Bearer {}'.format(access_token)
        }

    def get_user_details(self, access_token):
        params = {'schema': 'openid'}
        response = requests.get(self.USERINFO_URL, params=params,
                                headers=self.get_auth_headers(access_token))
        if response.status_code != 200:
            return {}
        return response.json()

    def get_user_id(self, access_token):
        return self.get_user_details(access_token).get('sub')
