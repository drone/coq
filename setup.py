from distutils.core import setup
from codecs import open
from os import path


def is_package(line):
    return line and not line.startswith(('--', 'git', '#', 'http'))


root_path = path.abspath(path.dirname(__file__))
with open(path.join(root_path, 'readme.md'), encoding='utf-8') as f:
    long_description = f.read()

with open('requirements.txt', encoding='utf-8') as reqs:
    install_requires = [l for l in reqs.read().split('\n') if is_package(l)]

setup(
    name='coq',
    version='0.1',
    author='French Drone Team',
    license='WTFPL',
    description='FranceConnect OAuth bridge.',
    long_description=long_description,
    install_requires=install_requires,
    classifiers=[
        'Intended Audience :: Developers',
        'Programming Language :: Python :: 3',
    ],
)
