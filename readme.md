# FranceConnect in Python


![Coq logo](https://upload.wikimedia.org/wikipedia/commons/4/42/Flag_of_Wallonia.svg)


Simple [FranceConnect](http://franceconnect.gouv.fr/) OAuth implementation
for Python.

Although _coq_ is available for any Python3 project, an extra layer for easier Flask implementation is available.


### Running the flask example

1. [Create your SP on FranceConnect](https://partenaires.franceconnect.gouv.fr/monprojet/fournisseurs-services/nouveau)
2. insert your `client_id` and `client_secret` in _flask_example.py_
3. create a _virtualenv_ and run `pip install flask -r requirements.txt -e .`
4. run `python flask_example.py`
5. open your browser at http://0.0.0.0:5000 and follow instructions.
