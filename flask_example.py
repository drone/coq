from flask import Flask

from coq.flask_coq import FranceConnectFlask, auth_required


fc = FranceConnectFlask(
    client_id='insert_client_id',
    client_secret='insert_client_secret',
    connected_url='http://localhost:5000/authorize/',
    disconnected_url='http://localhost:5000/disconnected/',
    )


app = Flask(__name__)
app.secret_key = 'secret'


@app.route('/')
def home():
    return 'Not much here. you should <a href=/login/?next=/me/>login</a>'


@app.route('/login/')
def login():
    return fc.login_view()


@app.route('/authorize/')
def authorize():
    return fc.authorize_view()


@app.route('/me/')
@auth_required(fc)
def profile():
    user = fc.get_user_id()
    return 'Your FranceConnect ID: {}<p><a href=/logout/>Logout</a></p>'.format(
        str(user))


@app.route('/logout/')
def logout():
    return fc.logout_view()


@app.route('/disconnected/')
def disconnected():
    return '<p>Bye. Please <a href=/login/?next=/me/>connect again</a>.</p>'


if __name__ == '__main__':
    app.run(port=5000, debug=True)
